# Qdictionnaire

 A Dictionary of the French language which supports
 a rhyming dictionary, definitions, names, rare words , conjugations ,
 quotes etc..

 Qdictionnaire utilise les composants OpenSource suivants :
° Lexique 3 (http://www.lexique.org) pour la base lexicale la base phonémique qui a été repensée pour les besoins du logiciel, mais certaines informations de Lexique 3 sont utilisées comme la fréquence d'apparition des mots
° La liste des villes est issue du site de l'INSEE http://www.insee.fr/fr/methodes/nomenclatures
° Wiktionary (http://www.wiktionary.org) pour la base de définitions et de synonymes
° Wikiquote (http://www.wikiquote) pour 46000 citations
° fortunes-fr (http://www.fortunes-fr.org) pour 3000 citations
° la base de données SQLite (http://www.sqlite.org)
° l'environnement de développement QtCreator(http://qt.nokia.com)
° la librairie d'affichage QT5 (http://www.qt-project.org)
° le tout a été développé sous système Linux sous Xubuntu (http://www.xunbuntu.org)
° l'icone du programme a été faite grâce à Inkscape http//www.inkscape.org
° la génération des binaires pour Windows se fait au moyen d'un émulateur sous Linux de temps en temps VMWare de temps en temps Virtual Box de Sun
° les définitions des verbes conjugués et l'affichage des tables de conjugaisons sont dues au programme french-conjugator du projet verbiste (http://perso.b2b2c.ca/sarrazip/) 

